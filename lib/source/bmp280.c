/*
 * bmp280.c
 *
 * Created on: 
 * Author: Travis Scott
 */

/************************************************************************************************************************************/
/*                                                             Includes                                                             */
/************************************************************************************************************************************/

//----- Interface Declarations -----//

#include "bmp280.h"

//----- Libraries -----//

#include <stdint.h>
#include <stdlib.h>

//----- Common -----//


//----- Modules -----//
 

//----- Platform Specific -----//


/************************************************************************************************************************************/
/*                                                       Private Definitions                                                        */
/************************************************************************************************************************************/

#define _BMP280_ADDRESS_TEMPERATURE_XLSB 	0xFC
#define _BMP280_ADDRESS_TEMPERATURE_LSB 	0xFB
#define _BMP280_ADDRESS_TEMPERATURE_MSB 	0xFA
#define _BMP280_ADDRESS_PRESSURE_XLSB 		0xF9
#define _BMP280_ADDRESS_PRESSURE_LSB 		0xF8
#define _BMP280_ADDRESS_PRESSURE_MSB 		0xF7
#define _BMP280_ADDRESS_CONFIG 				0xF5
#define _BMP280_ADDRESS_MEASUREMENT_CONTROL 0xF4
#define _BMP280_ADDRESS_STATUS 				0xF3 
#define _BMP280_ADDRESS_RESET 				0xE0
#define _BMP280_ADDRESS_ID 					0xD0
#define _BMP280_ADDRESS_CALIBRATION			0x88

#define _BMP280_MEASUREMENT_CONTROL_MODE_SHIFT						0
#define _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_SHIFT		2
#define _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_SHIFT	5

#define _BMP280_MEASUREMENT_CONTROL_MODE_SLEEP						0x0
#define _BMP280_MEASUREMENT_CONTROL_MODE_FORCED						0x1
#define _BMP280_MEASUREMENT_CONTROL_MODE_NORMAL						0x3

#define _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_OFF			0x0
#define _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_1 			0x1
#define _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_2			0x2
#define _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_4			0x3
#define _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_8			0x4
#define _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_16			0x5

#define _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_OFF 		0x0
#define _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_1 		0x1
#define _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_2 		0x2
#define _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_4 		0x3
#define _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_8 		0x4
#define _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_16		0x5

#define _BMP280_CONFIG_FILTER_COEFFICIENT_SHIFT 					2
#define _BMP280_CONFIG_STANDBY_SHIFT								5

#define _BMP280_CONFIG_FILTER_COEFFICIENT_OFF						0x0
#define _BMP280_CONFIG_FILTER_COEFFICIENT_2							0x1
#define _BMP280_CONFIG_FILTER_COEFFICIENT_4							0x2
#define _BMP280_CONFIG_FILTER_COEFFICIENT_8							0x3
#define _BMP280_CONFIG_FILTER_COEFFICIENT_16						0x4

#define _BMP280_CONFIG_STANDBY_0_MILLISECONDS_5						0x0
#define _BMP280_CONFIG_STANDBY_62_MILLISECONDS_5					0x1
#define _BMP280_CONFIG_STANDBY_125_MILLISECONDS						0x2
#define _BMP280_CONFIG_STANDBY_250_MILLISECONDS						0x3
#define _BMP280_CONFIG_STANDBY_500_MILLISECONDS						0x4
#define _BMP280_CONFIG_STANDBY_1000_MILLISECONDS					0x5
#define _BMP280_CONFIG_STANDBY_2000_MILLISECONDS					0x6
#define _BMP280_CONFIG_STANDBY_4000_MILLISECOND						0x7

#define _BMP280_TEMPERATURE_SHIFT									4
#define _BMP280_PRESSURE_SHIFT										4


#define _BMP280_RESET_WORD					0xB6

/************************************************************************************************************************************/
/*                                                        Private Structures                                                        */
/************************************************************************************************************************************/

struct bmp280_device {
	spi_transfer_func spi_transfer;

	uint16_t dig_t1;
	int16_t dig_t2;
	int16_t dig_t3;
	uint16_t dig_p1;
	int16_t dig_p2;
	int16_t dig_p3;
	int16_t dig_p4;
	int16_t dig_p5;
	int16_t dig_p6;
	int16_t dig_p7;
	int16_t dig_p8;
	int16_t dig_p9;

	int32_t temperature_for_compensation;
};

/************************************************************************************************************************************/
/*                                                         Local Prototypes                                                         */
/************************************************************************************************************************************/

static int32_t compensate_temperature(struct bmp280_device* bmp280_device_s_ptr, int32_t raw_temperature_adc);
static uint32_t compensate_pressure(struct bmp280_device* bmp280_device_s_ptr, int32_t raw_pressure_adc);

/************************************************************************************************************************************/
/*                                                         Local Variables                                                          */
/************************************************************************************************************************************/


/************************************************************************************************************************************/
/*                                                       Interface Functions                                                        */
/************************************************************************************************************************************/

struct bmp280_device* BMP280_initialise(spi_transfer_func spi_transfer)
{
	struct bmp280_device* bmp280_device_s_ptr = malloc(sizeof(struct bmp280_device));

	//----- Bind SPI transfer -----//

	bmp280_device_s_ptr->spi_transfer = spi_transfer;

	//---- Load calibration data -----//

	uint8_t data_array[27];

	data_array[0] = _BMP280_ADDRESS_CALIBRATION;

	bmp280_device_s_ptr->spi_transfer(sizeof(data_array), data_array);

	// Store as calibration values
	bmp280_device_s_ptr->dig_t1 = data_array[1] | (data_array[2] << 8); 
	bmp280_device_s_ptr->dig_t2 = data_array[3] | (data_array[4] << 8); 
	bmp280_device_s_ptr->dig_t3 = data_array[5] | (data_array[6] << 8); 
	bmp280_device_s_ptr->dig_p1 = data_array[7] | (data_array[8] << 8); 
	bmp280_device_s_ptr->dig_p2 = data_array[9] | (data_array[10] << 8); 
	bmp280_device_s_ptr->dig_p3 = data_array[11] | (data_array[12] << 8); 
	bmp280_device_s_ptr->dig_p4 = data_array[13] | (data_array[14] << 8); 
	bmp280_device_s_ptr->dig_p5 = data_array[15] | (data_array[16] << 8); 
	bmp280_device_s_ptr->dig_p6 = data_array[17] | (data_array[18] << 8); 
	bmp280_device_s_ptr->dig_p7 = data_array[19] | (data_array[20] << 8); 
	bmp280_device_s_ptr->dig_p8 = data_array[21] | (data_array[22] << 8); 
	bmp280_device_s_ptr->dig_p9 = data_array[23] | (data_array[24] << 8);

	return bmp280_device_s_ptr;
}

uint8_t BMP280_get_id(struct bmp280_device* bmp280_device_s_ptr)
{
	uint8_t data_array[2];

	data_array[0] = _BMP280_ADDRESS_ID;

	bmp280_device_s_ptr->spi_transfer(sizeof(data_array), data_array);

	return data_array[1];
}

void BMP280_reset(struct bmp280_device* bmp280_device_s_ptr)
{
	uint8_t data_array[2];

	data_array[0] = ~(1 << 7) &  _BMP280_ADDRESS_RESET;
	data_array[1] = _BMP280_RESET_WORD;

	bmp280_device_s_ptr->spi_transfer(sizeof(data_array), data_array);
}

void BMP280_configure(struct bmp280_device* bmp280_device_s_ptr, enum bmp280_mode mode_e, enum bmp280_pressure_oversample pressure_oversample_e, enum bmp280_temperature_oversample temperature_oversample_e, enum bmp280_filter_coefficient filter_coefficient_e, enum bmp280_standby standby_e)
{
	uint8_t data_array[4];

	data_array[0] = ~(1 << 7) & _BMP280_ADDRESS_MEASUREMENT_CONTROL;

	switch (mode_e) {
		case BMP280_MODE_SLEEP:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_MODE_SLEEP << _BMP280_MEASUREMENT_CONTROL_MODE_SHIFT);
			break;
		case BMP280_MODE_FORCED:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_MODE_FORCED << _BMP280_MEASUREMENT_CONTROL_MODE_SHIFT);
			break;
		case BMP280_MODE_NORMAL:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_MODE_NORMAL << _BMP280_MEASUREMENT_CONTROL_MODE_SHIFT);
			break;
		default:
			break;
	}

	switch (pressure_oversample_e) {
		case BMP280_PRESSURE_OVERSAMPLE_OFF:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_OFF << _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_PRESSURE_OVERSAMPLE_1:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_1 << _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_PRESSURE_OVERSAMPLE_2:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_2 << _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_PRESSURE_OVERSAMPLE_4:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_4 << _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_PRESSURE_OVERSAMPLE_8:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_8 << _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_PRESSURE_OVERSAMPLE_16:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_16 << _BMP280_MEASUREMENT_CONTROL_PRESSURE_OVERSAMPLE_SHIFT);
			break;
		default:
			break;
	}

	switch (temperature_oversample_e) {
		case BMP280_TEMPERATURE_OVERSAMPLE_OFF:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_OFF << _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_TEMPERATURE_OVERSAMPLE_1:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_1 << _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_TEMPERATURE_OVERSAMPLE_2:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_2 << _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_TEMPERATURE_OVERSAMPLE_4:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_4 << _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_TEMPERATURE_OVERSAMPLE_8:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_8 << _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_SHIFT);
			break;
		case BMP280_TEMPERATURE_OVERSAMPLE_16:
			data_array[1] |= (_BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_16 << _BMP280_MEASUREMENT_CONTROL_TEMPERATURE_OVERSAMPLE_SHIFT);
			break;
		default:
			break;
	}

	data_array[2] = ~(1 << 7) | _BMP280_ADDRESS_CONFIG;

	switch (filter_coefficient_e) {
		case BMP280_FILTER_COEFFICIENT_OFF:
			data_array[3] |= (_BMP280_CONFIG_FILTER_COEFFICIENT_OFF << _BMP280_CONFIG_FILTER_COEFFICIENT_SHIFT);
			break;
		case BMP280_FILTER_COEFFICIENT_2:
			data_array[3] |= (_BMP280_CONFIG_FILTER_COEFFICIENT_2 << _BMP280_CONFIG_FILTER_COEFFICIENT_SHIFT);
			break;
		case BMP280_FILTER_COEFFICIENT_4:
			data_array[3] |= (_BMP280_CONFIG_FILTER_COEFFICIENT_4 << _BMP280_CONFIG_FILTER_COEFFICIENT_SHIFT);
			break;
		case BMP280_FILTER_COEFFICIENT_8:
			data_array[3] |= (_BMP280_CONFIG_FILTER_COEFFICIENT_8 << _BMP280_CONFIG_FILTER_COEFFICIENT_SHIFT);
			break;
		case BMP280_FILTER_COEFFICIENT_16:
			data_array[3] |= (_BMP280_CONFIG_FILTER_COEFFICIENT_16 << _BMP280_CONFIG_FILTER_COEFFICIENT_SHIFT);
			break;
		default:
			break;
	}

	switch (standby_e) {
		case BMP280_STANDBY_0_MILLISECONDS_5:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_0_MILLISECONDS_5 << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
		case BMP280_STANDBY_62_MILLISECONDS_5:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_62_MILLISECONDS_5 << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
		case BMP280_STANDBY_125_MILLISECONDS:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_125_MILLISECONDS << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
		case BMP280_STANDBY_250_MILLISECONDS:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_250_MILLISECONDS << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
		case BMP280_STANDBY_500_MILLISECONDS:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_500_MILLISECONDS << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
		case BMP280_STANDBY_1000_MILLISECONDS:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_1000_MILLISECONDS << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
		case BMP280_STANDBY_2000_MILLISECONDS:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_2000_MILLISECONDS << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
		case BMP280_STANDBY_4000_MILLISECONDS:
			data_array[3] |= (_BMP280_CONFIG_STANDBY_4000_MILLISECOND << _BMP280_CONFIG_STANDBY_SHIFT);
			break;
	}

	bmp280_device_s_ptr->spi_transfer(sizeof(data_array), data_array);
}

void BMP280_read_sensor_values(struct bmp280_device* bmp280_device_s_ptr, int32_t* temperature_centidegrees_ptr, uint32_t* pressure_pascals_ptr)
{
	uint8_t data_array[7];

	data_array[0] = _BMP280_ADDRESS_PRESSURE_MSB;

	bmp280_device_s_ptr->spi_transfer(sizeof(data_array), data_array);

	int32_t raw_pressure = ((data_array[1] << 16) | (data_array[2] << 8) | (data_array[3] << 0)) >> _BMP280_PRESSURE_SHIFT;;
	int32_t raw_temperature = ((data_array[4] << 16) | (data_array[5] << 8) | (data_array[6] << 0)) >> _BMP280_TEMPERATURE_SHIFT;

	*temperature_centidegrees_ptr = compensate_temperature(bmp280_device_s_ptr, raw_temperature);
	*pressure_pascals_ptr = compensate_pressure(bmp280_device_s_ptr, raw_pressure);
}

/************************************************************************************************************************************/
/*                                                         Local Functions                                                          */
/************************************************************************************************************************************/

static int32_t compensate_temperature(struct bmp280_device* bmp280_device_s_ptr, int32_t raw_temperature_adc)
{
	int32_t var1;
	int32_t var2;
	int32_t temperature_centidegrees;

	var1 = ((((raw_temperature_adc >> 3) - ((int32_t)bmp280_device_s_ptr->dig_t1 << 1))) * ((int32_t)bmp280_device_s_ptr->dig_t2)) >> 11;
	var2 = (((((raw_temperature_adc >> 4) - ((int32_t)bmp280_device_s_ptr->dig_t1)) * ((raw_temperature_adc >> 4) - ((int32_t)bmp280_device_s_ptr->dig_t1))) >> 12) * ((int32_t)bmp280_device_s_ptr->dig_t3)) >> 14;

	bmp280_device_s_ptr->temperature_for_compensation = var1 + var2;

	temperature_centidegrees = (bmp280_device_s_ptr->temperature_for_compensation * 5 + 128) >> 8;

	return temperature_centidegrees;
}

static uint32_t compensate_pressure(struct bmp280_device* bmp280_device_s_ptr, int32_t raw_pressure_adc)
{
	int32_t var1;
	int32_t var2;
	uint32_t pressure_pascals;

	var1 = (((int32_t)bmp280_device_s_ptr->temperature_for_compensation) >> 1) - (int32_t)64000;
	var2 = (((var1 >> 2) * (var1 >> 2)) >> 11 ) * ((int32_t)bmp280_device_s_ptr->dig_p6);
	var2 = var2 + ((var1 *((int32_t)bmp280_device_s_ptr->dig_p5)) << 1);
	var2 = (var2 >> 2) + (((int32_t)bmp280_device_s_ptr->dig_p4) << 16);
	var1 = (((bmp280_device_s_ptr->dig_p3 * (((var1 >> 2) * (var1 >> 2)) >> 13 )) >> 3) + ((((int32_t)bmp280_device_s_ptr->dig_p2) * var1) >> 1)) >> 18;
	var1 = ((((32768 + var1)) * ((int32_t)bmp280_device_s_ptr->dig_p1)) >> 15);

	if (var1 == 0) {
		return 0; // avoid exception caused by division by zero
	}

	pressure_pascals = (((uint32_t)(((int32_t)1048576) - raw_pressure_adc) - (var2 >> 12))) * 3125;

	if (pressure_pascals < 0x80000000)	{
		pressure_pascals = (pressure_pascals << 1) / ((uint32_t)var1);
	} else {
		pressure_pascals = (pressure_pascals / (uint32_t)var1) * 2;
	}

	var1 = (((int32_t)bmp280_device_s_ptr->dig_p9) * ((int32_t)(((pressure_pascals >> 3) * (pressure_pascals >> 3)) >> 13))) >> 12;
	var2 = (((int32_t)(pressure_pascals >> 2)) * ((int32_t)bmp280_device_s_ptr->dig_p8)) >> 13;
	
	pressure_pascals = (uint32_t)((int32_t)pressure_pascals + ((var1 + var2 + bmp280_device_s_ptr->dig_p7) >> 4));
	
	return pressure_pascals;
}

/************************************************************************************************************************************/
/*                                                    Interrupt Service Routines                                                    */
/************************************************************************************************************************************/