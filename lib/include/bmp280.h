/*
 * bmp280.h
 *
 * Created on: 
 * Author: Travis Scott
 */

#ifndef BMP280_H
#define BMP280_H

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************************************************************************/
/*                                                     Header Required Includes                                                     */
/************************************************************************************************************************************/

//----- Libraries -----//

#include <stdint.h>

//----- Common -----//


//----- Modules -----//


//----- Platform Specific -----//


/************************************************************************************************************************************/
/*                                                       Public Definitions                                                         */
/************************************************************************************************************************************/


/************************************************************************************************************************************/
/*                                                        Public Structures                                                         */
/************************************************************************************************************************************/

struct bmp280_device;

typedef void (*spi_transfer_func)(uint32_t num_bytes, uint8_t* data_array);

enum bmp280_mode {
	BMP280_MODE_SLEEP,
	BMP280_MODE_FORCED,
	BMP280_MODE_NORMAL
};

enum bmp280_pressure_oversample {
	BMP280_PRESSURE_OVERSAMPLE_OFF,
	BMP280_PRESSURE_OVERSAMPLE_1,
	BMP280_PRESSURE_OVERSAMPLE_2,
	BMP280_PRESSURE_OVERSAMPLE_4,
	BMP280_PRESSURE_OVERSAMPLE_8,
	BMP280_PRESSURE_OVERSAMPLE_16,
};

enum bmp280_temperature_oversample {
	BMP280_TEMPERATURE_OVERSAMPLE_OFF,
	BMP280_TEMPERATURE_OVERSAMPLE_1,
	BMP280_TEMPERATURE_OVERSAMPLE_2,
	BMP280_TEMPERATURE_OVERSAMPLE_4,
	BMP280_TEMPERATURE_OVERSAMPLE_8,
	BMP280_TEMPERATURE_OVERSAMPLE_16,
};

enum bmp280_filter_coefficient {
	BMP280_FILTER_COEFFICIENT_OFF,
	BMP280_FILTER_COEFFICIENT_2,
	BMP280_FILTER_COEFFICIENT_4,
	BMP280_FILTER_COEFFICIENT_8,
	BMP280_FILTER_COEFFICIENT_16
};

enum bmp280_standby {
	BMP280_STANDBY_0_MILLISECONDS_5,
	BMP280_STANDBY_62_MILLISECONDS_5,
	BMP280_STANDBY_125_MILLISECONDS,
	BMP280_STANDBY_250_MILLISECONDS,
	BMP280_STANDBY_500_MILLISECONDS,
	BMP280_STANDBY_1000_MILLISECONDS,
	BMP280_STANDBY_2000_MILLISECONDS,
	BMP280_STANDBY_4000_MILLISECONDS
};

/************************************************************************************************************************************/
/*                                                         Global Variables                                                         */
/************************************************************************************************************************************/


/************************************************************************************************************************************/
/*                                                       Interface Prototypes                                                       */
/************************************************************************************************************************************/

struct bmp280_device* BMP280_initialise(spi_transfer_func spi_transfer);

void BMP280_configure(struct bmp280_device* bmp280_device_s_ptr, enum bmp280_mode mode_e, enum bmp280_pressure_oversample pressure_oversample_e, enum bmp280_temperature_oversample temperature_oversample_e, enum bmp280_filter_coefficient filter_coefficient_e, enum bmp280_standby standby_e);

uint8_t BMP280_get_id(struct bmp280_device* bmp280_device_ptr);

void BMP280_reset(struct bmp280_device* bmp280_device_s_ptr);

void BMP280_read_sensor_values(struct bmp280_device* bmp280_device_s_ptr, int32_t* temperature_centidegrees_ptr, uint32_t* pressure_pascals_ptr);

#ifdef __cplusplus
}
#endif

#endif /* BMP280_H */